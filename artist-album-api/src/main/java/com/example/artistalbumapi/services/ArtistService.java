package com.example.artistalbumapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.artistalbumapi.models.Album;
import com.example.artistalbumapi.models.Artist;

@Service
public class ArtistService {

    public List<Artist> createArtistList(ArrayList<Album> albumList) {
        ArrayList<Artist> artistList = new ArrayList<>();
        Artist artist1 = new Artist(1, "Taylor", albumList);
        Artist artist2 = new Artist(2, "Mint", albumList);
        Artist artist3 = new Artist(3, "Adam", albumList);
        artistList.addAll(Arrays.asList(artist1, artist2, artist3));

        for (Artist artist : artistList) {
            System.out.println(artist);
        }
        return artistList;
    }

    public Artist getArtistInfo(int id, ArrayList<Artist> artistList) {
        for (Artist artist : artistList) {
            if (artist.getId() == id) {
                return artist;
            }
        }
        return null;
    }
}
